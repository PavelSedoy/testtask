<?php

use \RedBeanPHP\R as R;
require __DIR__ . '/vendor/autoload.php';
R::setup('mysql:host=localhost;dbname=testTask;charset=utf8', 'test', 'test');
R::freeze(true);

$sendMessage = R::dispense( 'message' );
$sendMessage->name = prepareInput($_POST['name']);
$sendMessage->phone = prepareInput($_POST['phone']);
$sendMessage->email = prepareInput($_POST['email']);
$sendMessage->message = prepareInput($_POST['message']);

R::store( $sendMessage );

function prepareInput($post)
{
	return trim(htmlspecialchars($post));
}