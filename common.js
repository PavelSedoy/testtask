$(document).ready(function() {
	$('.send').click(function() {
		var name = $('#name').val();
		var phone = $('#phone').val();
		var email = $('#email').val();
		var message = $('#message').val();

		if (name == '' || phone == '' || email == '' || message == '') {
			$('.error').css('display', 'block');

			return false;
		}

		$.ajax({
			url: 'addMessage.php',
			type: 'POST',
			data: {
				'name' : name,
				'phone' : phone,
				'email' : email,
				'message' : message
			},
			success: function(data) {
				console.log(data);
				if (data == '') {
					$('#sendMessage').css('display', 'none');
					$('.success').css('display', 'block');
					$('.error').css('display', 'none');
				} else {
					$('.error').css('display', 'block');
				}
			}
		});
		
		return false;
	});
});